package com.ripley.jesus.leon.ApiTest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ripley.jesus.leon.ApiTest.model.CreditoClientes;

/**
 * @author jleonran
 *
 */
@Repository
public interface CreditoClientesRepository extends JpaRepository<CreditoClientes, String> {

	/**
	 * Metodo para buscar un credito por su numero de folio
	 * 
	 * @param folio
	 * @return CreditoClientes folio que se consigue
	 */
	public CreditoClientes findByFolio(String folio);

}
