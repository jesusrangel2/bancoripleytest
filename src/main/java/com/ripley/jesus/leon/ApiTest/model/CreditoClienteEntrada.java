package com.ripley.jesus.leon.ApiTest.model;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import lombok.Data;

@Data
public class CreditoClienteEntrada {

	@Length(max = 10)
	private String rut;
	@NotNull
	private Integer montoCredito;
	@NotNull
	private int cuotas;
	
}
