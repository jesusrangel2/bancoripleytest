package com.ripley.jesus.leon.ApiTest.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.Length;

import lombok.Data;

@Data
@Entity
@Table(name = "credito_clientes")
public class CreditoClientes {

	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid2")
	private String folio;
	@NotNull
	@Length(max = 10)
	private String rut;
	@NotNull
	private Date fecha;
	@NotNull
	private Integer montoCredito;
	@NotNull
	private int cuotas;
	@NotNull
	private Double tasaInteres;
	@NotNull
	private Double valorCuota;
}
