package com.ripley.jesus.leon.ApiTest.utils;

import static org.decimal4j.util.DoubleRounder.round;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.ripley.jesus.leon.ApiTest.constants.CreditoConstants;
import com.ripley.jesus.leon.ApiTest.exception.InvalidCreditRequestException;
import com.ripley.jesus.leon.ApiTest.model.CreditoClienteEntrada;
import com.ripley.jesus.leon.ApiTest.model.CreditoClientes;

/**
 * @author jleonran
 *
 */
@Component
public class Utils {

	private static final Logger logger = LoggerFactory.getLogger(Utils.class);

	@Value("${credito.min_cuotas}")
	public int MIN_COUTAS;
	@Value("${credito.max_cuotas}")
	public int MAX_COUTAS;

	@Value("${credito.intereses.couta_1}")
	private double INTERES_CUOTA_1;
	@Value("${credito.intereses.couta_3}")
	private double INTERES_CUOTA_3;
	@Value("${credito.intereses.couta_6_12}")
	private double INTERES_CUOTA_6_12;
	@Value("${credito.intereses.couta_13_24}")
	private double INTERES_CUOTA_13_24;
	@Value("${credito.intereses.couta_25_36}")
	private double INTERES_CUOTA_25_36;
	@Value("${credito.intereses.couta_37_48}")
	private double INTERES_CUOTA_37_48;
	@Value("${credito.intereses.couta_49_60}")
	private double INTERES_CUOTA_49_60;

	/**
	 * Metodo que segun la cantidad de cuotas hace set al objeto credito la tasa de
	 * interese y el valor cuota que se calculo con dicho interes
	 * 
	 * @param credito
	 */
	public void calcularInteres(CreditoClientes credito) {
		logger.info("Calcular los intereses del credito ");
		if (credito.getCuotas() == 1) {
			credito.setTasaInteres(INTERES_CUOTA_1);
			credito.setValorCuota(Double.valueOf(credito.getMontoCredito()));
		} else if (credito.getCuotas() == 3) {
			credito.setTasaInteres(INTERES_CUOTA_3);
			credito.setValorCuota(obtenerValorCuota(credito.getMontoCredito(), credito.getCuotas(), INTERES_CUOTA_3));
		} else if (credito.getCuotas() >= 6 && credito.getCuotas() <= 12) {
			credito.setTasaInteres(INTERES_CUOTA_6_12);
			credito.setValorCuota(
					obtenerValorCuota(credito.getMontoCredito(), credito.getCuotas(), INTERES_CUOTA_6_12));
		} else if (credito.getCuotas() >= 13 && credito.getCuotas() <= 24) {
			credito.setTasaInteres(INTERES_CUOTA_13_24);
			credito.setValorCuota(
					obtenerValorCuota(credito.getMontoCredito(), credito.getCuotas(), INTERES_CUOTA_13_24));
		} else if (credito.getCuotas() >= 25 && credito.getCuotas() <= 36) {
			credito.setTasaInteres(INTERES_CUOTA_25_36);
			credito.setValorCuota(
					obtenerValorCuota(credito.getMontoCredito(), credito.getCuotas(), INTERES_CUOTA_25_36));
		} else if (credito.getCuotas() >= 37 && credito.getCuotas() <= 48) {
			credito.setTasaInteres(INTERES_CUOTA_37_48);
			credito.setValorCuota(
					obtenerValorCuota(credito.getMontoCredito(), credito.getCuotas(), INTERES_CUOTA_37_48));
		} else if (credito.getCuotas() >= 49 && credito.getCuotas() <= 60) {
			credito.setTasaInteres(INTERES_CUOTA_49_60);
			credito.setValorCuota(
					obtenerValorCuota(credito.getMontoCredito(), credito.getCuotas(), INTERES_CUOTA_49_60));
		}

	}

	/**
	 * Metodo que convierte el obtejto de entrada del servicio al objeto real que se
	 * va a enviar a la base de datos donde se completa los intreses que se van a
	 * pagar por el credito
	 * 
	 * @param credito objeto de request del servicio que tiene menos datos de los
	 *                necesarios para BD
	 * @return creditoReq objeto que se envia a la base de datos
	 */
	public CreditoClientes convertEntrada(CreditoClienteEntrada credito) {
		logger.info("Convert request de entrada del servicio en request para la base de datos");
		CreditoClientes creditoReq = new CreditoClientes();

		creditoReq.setCuotas(credito.getCuotas());
		creditoReq.setFecha(new Date());
		creditoReq.setMontoCredito(credito.getMontoCredito());
		creditoReq.setRut(credito.getRut());
		calcularInteres(creditoReq);

		return creditoReq;
	}

	/**
	 * Metodo que se encarga de validar la catidad de cuotas ingresadas, el rut
	 * ingresado y el monto del credito ingresado
	 * 
	 * @param request
	 * @throws InvalidCreditRequestException
	 */
	public void validFormatRequest(CreditoClienteEntrada request) throws InvalidCreditRequestException {
		logger.info("Validando los campos minimos del request para agregar un credito");
		if (!validarCuotas(request.getCuotas())) {
			throw new InvalidCreditRequestException(CreditoConstants.MSJ_CUOTAS_INVALIDAS);
		} else if (!validarRut(request.getRut())) {
			throw new InvalidCreditRequestException(CreditoConstants.MSJ_RUT_INVALIDO);
		} else if (request.getMontoCredito() < 1) {
			throw new InvalidCreditRequestException(CreditoConstants.MSJ_MONTO_INVALIDO);
		}
	}

	/**
	 * Metodo que se encarga de validar la catidad de cuotas ingresadas, el rut
	 * ingresado y el monto del credito ingresado
	 * 
	 * @param request
	 * @throws InvalidCreditRequestException
	 */
	public void validFormatRequest(CreditoClientes request) throws InvalidCreditRequestException {
		logger.info("Validando los campos minimos del request para modificar un credito");
		if (!validarCuotas(request.getCuotas())) {
			throw new InvalidCreditRequestException(CreditoConstants.MSJ_CUOTAS_INVALIDAS);
		} else if (!validarRut(request.getRut())) {
			throw new InvalidCreditRequestException(CreditoConstants.MSJ_RUT_INVALIDO);
		} else if (request.getMontoCredito() < 1) {
			throw new InvalidCreditRequestException(CreditoConstants.MSJ_MONTO_INVALIDO);
		}
	}

	/**
	 * Metodo que valida el monto de la cuota y el porcentje de interes que se
	 * aplica cuando se hace una actualizacion de un credito
	 * 
	 * @param credito objeto credito que se quiere actualizar
	 * @throws InvalidCreditRequestException
	 */
	public void validCuotasUpdate(CreditoClientes credito) throws InvalidCreditRequestException {
		logger.info("Validando campos de tasa de interes y valor cuota para los update");
		CreditoClientes auxCredito = new CreditoClientes();
		auxCredito.setCuotas(credito.getCuotas());
		auxCredito.setMontoCredito(credito.getMontoCredito());
		calcularInteres(auxCredito);
		if (auxCredito.getTasaInteres() > credito.getTasaInteres()
				|| auxCredito.getTasaInteres() < credito.getTasaInteres()) {
			throw new InvalidCreditRequestException(CreditoConstants.MSJ_TASA_INTERES_INVALIDO);
		} else if (auxCredito.getValorCuota() < credito.getValorCuota()
				|| auxCredito.getValorCuota() > credito.getValorCuota()) {
			throw new InvalidCreditRequestException(CreditoConstants.MSJ_VALOR_CUOTA_INVALIDO);
		}
	}

	/**
	 * Metodo que calcula cuanto es el valor de la cuota segun la cantidad de cuotas
	 * y el interess
	 * 
	 * @param monto   monto ingresado para solicitar el credito
	 * @param cuotas  cantidad de cuotas en las que se va a pagar el monto
	 * @param interes cantidad de interes que se aplica al monto del credito
	 * @return monto de la cuota a pagar
	 */
	private Double obtenerValorCuota(Integer monto, int cuotas, double interes) {
		logger.info("Calculando el valor de la cuota para el monto {} y las cuotas {}", monto, cuotas);
		return round((((monto * interes) / 100) + monto) / cuotas, 3);
	}

	/**
	 * Metodo que se encarga de validar que las cuotas ingresadas vayan entre el
	 * minimo y el maximo permitido
	 * 
	 * @param cuotas numero de cuotas ingresado
	 * @return boolean indica si la cantidad de cuotas es valida
	 */
	private boolean validarCuotas(int cuotas) {
		logger.info("Validando la canitdad de cuotas ingresadas");
		return cuotas > MIN_COUTAS && cuotas < MAX_COUTAS;
	}

	/**
	 * Metodo que se encarga de validar que un rut sea valido.
	 * 
	 * @param rut Rut que se desea validar
	 * @return validacion indica si el rut es valido
	 */
	private boolean validarRut(String rut) {
		logger.info("Validando el rut ingresado");
		boolean validacion = false;
		try {
			rut = rut.toUpperCase();
			rut = rut.replace(".", "");
			rut = rut.replace("-", "");
			int rutAux = Integer.parseInt(rut.substring(0, rut.length() - 1));

			char dv = rut.charAt(rut.length() - 1);

			int m = 0, s = 1;
			for (; rutAux != 0; rutAux /= 10) {
				s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
			}
			if (dv == (char) (s != 0 ? s + 47 : 75)) {
				validacion = true;
			}

		} catch (java.lang.NumberFormatException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return validacion;
	}

}
