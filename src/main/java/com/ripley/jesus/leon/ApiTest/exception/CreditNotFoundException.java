package com.ripley.jesus.leon.ApiTest.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.ripley.jesus.leon.ApiTest.constants.CreditoConstants;

/**
 * @author jleonran
 *
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class CreditNotFoundException extends RuntimeException {
	private static final long serialVersionUID = 284279701638486458L;

	/**
	 * Excepcion para cuando no existe un credito
	 */
	public CreditNotFoundException() {
		super(CreditoConstants.MSJ_CREDITO_NO_ENCONTRADO);
	}
}
