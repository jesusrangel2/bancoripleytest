package com.ripley.jesus.leon.ApiTest.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ripley.jesus.leon.ApiTest.exception.CreditNotFoundException;
import com.ripley.jesus.leon.ApiTest.model.CreditoClienteDeleteResponse;
import com.ripley.jesus.leon.ApiTest.model.CreditoClienteEntrada;
import com.ripley.jesus.leon.ApiTest.model.CreditoClientes;
import com.ripley.jesus.leon.ApiTest.service.CreditoClientesServices;
import com.ripley.jesus.leon.ApiTest.utils.Utils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author jleonran
 *
 */
@RestController
@RequestMapping("/api/credito/")
@Api(tags = "Credito Clientes")
public class CreditoClientesController {

	private static final Logger logger = LoggerFactory.getLogger(CreditoClientesController.class);

	@Autowired
	private CreditoClientesServices creditoService;

	@Autowired
	private Utils creditoUtils;

	/**
	 * Metodo que resuleve las peticiones get para consultar todos los creditos en
	 * la base de datos retorna una lista de los creditos encontrados
	 * 
	 * @return ResponseEntity<List<CreditoClientes>>
	 */
	@GetMapping
	@ApiOperation(value = "Listar creditos", notes = "Servicio listar todos los creditos")
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Creditos encontrados"),
			@ApiResponse(code = 400, message = "Creditos no encontrados") })
	public ResponseEntity<List<CreditoClientes>> findAll() {
		return new ResponseEntity<List<CreditoClientes>>(creditoService.findAll(), HttpStatus.OK);
	}

	/**
	 * Metodo que resuelve las peticiones post para agregar un credito nuevo a la
	 * base de datos
	 * 
	 * @param credito Credito que se desea solicitar
	 * @return ResponseEntity<CreditoClientes> credito ingresado con los valores del
	 *         interes y el valor cuotacalculados
	 */
	@PostMapping
	@ApiOperation(value = "Crear credito", notes = "Servicio para crear un nuevo credito para un cliente")
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Credito creado correctamente"),
			@ApiResponse(code = 400, message = "Solicitud Invalida") })
	public ResponseEntity<CreditoClientes> addCredit(@RequestBody @Valid CreditoClienteEntrada credito) {
		logger.info("Añadir credito");

		if (credito == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		creditoUtils.validFormatRequest(credito);
		CreditoClientes creditoReq = creditoUtils.convertEntrada(credito);
		return new ResponseEntity<CreditoClientes>(creditoService.addCredit(creditoReq), HttpStatus.CREATED);

	}

	/**
	 * Metodo que resuelve las peticiones get para consultar un credito por su
	 * numero de folio
	 * 
	 * @param folio String del folio a consultar
	 * @return ResponseEntity<CreditoClientes> Credito consultado si existe
	 */
	@GetMapping("/{folio}")
	@ApiOperation(value = "Consultar credito", notes = "Servicio para conusltar un credito por su número de folio")
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Credito creado correctamente"),
			@ApiResponse(code = 400, message = "Solicitud Invalida"),
			@ApiResponse(code = 404, message = "Credito no encontrado") })
	public ResponseEntity<CreditoClientes> find(@PathVariable("folio") String folio) {
		if (folio == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		CreditoClientes credito = creditoService.findByFolio(folio);
		if (credito == null) {
			throw new CreditNotFoundException();
		}
		return new ResponseEntity<CreditoClientes>(creditoService.findByFolio(folio), HttpStatus.OK);
	}

	/**
	 * Metodo que resuelve las peticiones put para actualizar un credito
	 * 
	 * @param folio   folio que se desea actualizar
	 * @param credito informacion del credito que se va a actualizar
	 * @return ResponseEntity<CreditoClientes> Credito modificado
	 */
	@PutMapping("/{folio}")
	@ApiOperation(value = "Actualizar credito", notes = "Servicio para actualizar el credito de un cliente")
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Credito actualizado correctamente"),
			@ApiResponse(code = 404, message = "Credito no encontrado") })
	public ResponseEntity<CreditoClientes> updateCredit(@PathVariable("folio") String folio,
			@Valid @RequestBody CreditoClientes credito) {

		if (folio == null || credito == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		creditoUtils.validFormatRequest(credito);
		creditoUtils.validCuotasUpdate(credito);
		return new ResponseEntity<CreditoClientes>(creditoService.updateCredit(folio, credito), HttpStatus.OK);

	}

	/**
	 * Metodo que resuelve las peticiones delete para eliminar un credito de la base
	 * de datos
	 * 
	 * @param folio numero de folio del credito a eliminar
	 * @return CreditoClienteDeleteResponse objeto con mensaje de respuesta y numero
	 *         de folio que se elimino
	 */
	@DeleteMapping("/{folio}")
	@ApiOperation(value = "Eliminar credito", notes = "Servicio para eliminar credito de un cliente")
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Credito eliminado correctamente"),
			@ApiResponse(code = 404, message = "Credito no encontrado") })
	public ResponseEntity<CreditoClienteDeleteResponse> deleteClient(@PathVariable("folio") String folio) {

		if (folio == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<CreditoClienteDeleteResponse>(creditoService.deleteCredit(folio), HttpStatus.OK);
	}

}
