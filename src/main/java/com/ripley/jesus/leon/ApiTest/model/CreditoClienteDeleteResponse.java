package com.ripley.jesus.leon.ApiTest.model;

import lombok.Data;

@Data
public class CreditoClienteDeleteResponse {
	private String folio;
	private String message;
}
