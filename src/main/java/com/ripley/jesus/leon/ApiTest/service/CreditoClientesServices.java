package com.ripley.jesus.leon.ApiTest.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ripley.jesus.leon.ApiTest.constants.CreditoConstants;
import com.ripley.jesus.leon.ApiTest.exception.CreditNotFoundException;
import com.ripley.jesus.leon.ApiTest.model.CreditoClienteDeleteResponse;
import com.ripley.jesus.leon.ApiTest.model.CreditoClientes;
import com.ripley.jesus.leon.ApiTest.repository.CreditoClientesRepository;

/**
 * @author jleonran
 *
 */
@Service
public class CreditoClientesServices {

	private static final Logger logger = LoggerFactory.getLogger(CreditoClientesServices.class);

	@Autowired
	private CreditoClientesRepository creditoClientesRepository;

	/**
	 * Metodo para crear un credito
	 * 
	 * @param CreditoClientes
	 * @return CreditoClientes
	 */
	@Transactional
	public CreditoClientes addCredit(CreditoClientes credito) {
		logger.debug("Inicia el metodo addCredit con los datos {}", credito);
		return creditoClientesRepository.save(credito);
	}

	/**
	 * Metodo para actualizar un credito
	 * 
	 * @param CreditoClientes
	 * @return CreditoClientes
	 */
	@Transactional
	public CreditoClientes updateCredit(String folio, CreditoClientes credito) {
		logger.debug("Inicia el metodo updateCredit con los datos de credito {} y el folio {}", credito, folio);
		CreditoClientes creditoActual = creditoClientesRepository.findByFolio(folio);
		if (creditoActual == null) {
			throw new CreditNotFoundException();
		}
		creditoActual.setMontoCredito(credito.getMontoCredito());
		creditoActual.setTasaInteres(credito.getTasaInteres());
		creditoActual.setValorCuota(credito.getValorCuota());
		return creditoClientesRepository.save(creditoActual);
	}

	/**
	 * Metodo que se utiliza para eliminar un credito
	 * 
	 * @param CreditoCliente
	 * 
	 */
	@Transactional
	public CreditoClienteDeleteResponse deleteCredit(String folio) {
		logger.debug("Inicia el metodo deleteCredit el folio {}", folio);
		CreditoClientes credito = creditoClientesRepository.findByFolio(folio);
		if (credito == null) {
			throw new CreditNotFoundException();
		}
		creditoClientesRepository.delete(credito);

		CreditoClienteDeleteResponse delete = new CreditoClienteDeleteResponse();
		delete.setFolio(folio);
		delete.setMessage(CreditoConstants.MSJ_DELETE_SUCCESS);

		return delete;
	}

	/**
	 * Metodo que se utiliza para buscar todos los creditos almacenados
	 * 
	 * @param String folio
	 * @return Client
	 */
	public List<CreditoClientes> findAll() {
		logger.debug("Inicia el metodo findAll");
		return creditoClientesRepository.findAll();
	}

	/**
	 * Metodo que se utiliza para buscar un credito por el numero de folio
	 * 
	 * @param folio
	 * @return
	 */
	public CreditoClientes findByFolio(String folio) {
		logger.debug("Inicia el metodo findByFolio con el folio {}", folio);
		return creditoClientesRepository.findByFolio(folio);
	}
}
