package com.ripley.jesus.leon.ApiTest.constants;

public class CreditoConstants {

	public static final String MSJ_RUT_INVALIDO = "El rut ingresado no es valido";
	public static final String MSJ_CUOTAS_INVALIDAS = "La cantidad de cuotas ingresadas es invalida, debe ser un valor entre 1 y 60";
	public static final String MSJ_MONTO_INVALIDO = "El Monto debe ser mayor a 1";
	public static final String MSJ_TASA_INTERES_INVALIDO = "La tasa de interes es incorrecta para el número de cuotas ingresadas";
	public static final String MSJ_VALOR_CUOTA_INVALIDO = "El valor cuota es incorrecta para el monto ingresado";
	public static final String MSJ_CREDITO_NO_ENCONTRADO = "El crédito ingresado no existe";
	public static final String MSJ_DELETE_SUCCESS = "Credito eliminado con exito";

}
