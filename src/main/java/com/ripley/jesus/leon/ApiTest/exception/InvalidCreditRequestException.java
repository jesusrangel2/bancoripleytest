package com.ripley.jesus.leon.ApiTest.exception;

/**
 * @author jleonran
 *
 */
public class InvalidCreditRequestException extends RuntimeException {

	private static final long serialVersionUID = -5117676979778177329L;

	/**
	 * @param msj
	 */
	public InvalidCreditRequestException(String msj) {
		super(msj);
	}

}
